{
  description = "nixify-rust";

  nixConfig.bash-prompt = "\[nix-develop nixify-rust\]$ ";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, utils, naersk, fenix }:
    utils.lib.eachSystem [ "x86_64-linux" "aarch64-darwin" "aarch64-linux" ]
      (system:
        let
          name = "nixify-rust";

          pkgs = nixpkgs.legacyPackages."${system}";
          naersk-lib = naersk.lib."${system}";
          fenix-pkgs = fenix.packages."${system}".stable.toolchain;
          cargoToml = (builtins.fromTOML (builtins.readFile ./Cargo.toml));

          ubuntu2004-img = pkgs.dockerTools.pullImage {
            name = "ubuntu";
            imageName = "ubuntu";
            imageDigest = "sha256:7cc0576c7c0ec2384de5cbf245f41567e922aab1b075f3e8ad565f508032df17";
            finalImageTag = "focal-20211006";
            sha256 = "sha256-UrUt7DNiHSWl6lORC9aDqG4rKIrcCIF/WPzw2GhWhH8=";
          };

          nativeBuildInputs = [
            pkgs.libiconv
            pkgs.pkg-config
            pkgs.cmake
            pkgs.openssl
            pkgs.protobuf
            pkgs.grpc

            fenix-pkgs
          ];

          buildEnvVars = {
            GRPCIO_SYS_USE_PKG_CONFIG = "1";
            CXXFLAGS = "-O2 -fPIC -g -fno-omit-frame-pointer";
          };
        in
        rec {
          # `nix build`
          packages."${name}-bin" = naersk-lib.buildPackage
            ({
              inherit nativeBuildInputs;
              inherit name;

              root = ./.;
              release = true;
            } // buildEnvVars);

          packages."${name}-docker" = pkgs.dockerTools.buildImage {
            fromImage = ubuntu2004-img;
            name = "${name}";
            tag = "${cargoToml.package.version}";
            config = {
              ExposedPorts = {
                "50052/tcp" = { };
              };
              Cmd = [ "${packages.nixify-rust-bin}/bin/nixify-rust" ];
            };
          };

          # `nix develop`
          devShell = pkgs.mkShell
            ({
              inherit nativeBuildInputs;
            } // buildEnvVars);
        });
}
