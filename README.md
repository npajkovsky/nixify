# Nixify Rust flake

A Rust template project with fully functional nix shell, builds and docker image.

```bash
# Dev shell
nix develop

# or run via cargo
nix develop -c cargo run

# build binary
nix build .#nixify-rust-bin

# build docker image
nix build .#nixify-rust-docker
```

## No Legacy Nix
